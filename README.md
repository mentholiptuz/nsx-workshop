# Welcome to VMware NSX Knowledge Transfer Workshop - Metro Systems Corporation

**Hands On Labs Series**
* HOL-1803-01-NET - VMware NSX - Getting Started
    * M2 Logical Switching
    * M3 Logical Routing
    * M4 Edge Services Gateway

* HOL-1892-01-CHG - VMware NSX - Challenge Lab
    * M5 Troubleshooting
    
## Getting Started

1. Hit link https://labs.hol.vmware.com/ and register.
2. Go to lab number and enroll lab.

## Prerequisites

1. Browser Google Chorme 65+, Mozilla Firefox 60+.
2. Internet Connection.