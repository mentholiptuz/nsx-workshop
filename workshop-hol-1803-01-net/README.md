# Welcome to VMware NSX Knowledge Transfer Workshop - HOL-1803-01-NET - VMware NSX - Getting Started

**Hands On Labs Series**
* HOL-1803-01-NET - VMware NSX - Getting Started
    * M2 Logical Switching
    * M3 Logical Routing
    * M4 Edge Services Gateway
    
## Getting Started

1. Hit link https://labs.hol.vmware.com/ and register.
2. Go to lab HOL-1803-01-NET - VMware NSX - Getting Started.

## Prerequisites

1. Browser Google Chorme 65+, Mozilla Firefox 60+.
2. Internet Connection.

## Lab Guide
```
vCenter Server : https://vcsa-01a.corp.local
Username : administrator@corp.local
Password : VMware1!

Logical and VM Network
OSPF Peer        192.168.5.0/29
Transit Network  192.168.9.0/24
Web VM Network   172.16.10.0/24
App VM Network	 172.16.20.0/24
DB VM Network 	 172.16.30.0/24
web-01a          172.16.10.11
web-02a          172.16.10.12
web-tier-vip     172.16.10.10
```

## Lab Topology

![](lab-topology-1.png)