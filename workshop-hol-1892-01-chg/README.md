# Welcome to VMware NSX Knowledge Transfer Workshop - HOL-1892-01-CHG - VMware NSX - Challenge Lab

**Hands On Labs Series**
* HOL-1892-01-CHG - VMware NSX - Challenge Lab
    * M5 Troubleshooting
    
## Getting Started

1. Hit link https://labs.hol.vmware.com/ and register.
2. Go to lab HOL-1892-01-CHG  - VMware NSX - Getting Started.

## Prerequisites

1. Browser Google Chorme 65+, Mozilla Firefox 60+.
2. Internet Connection.

## Lab Guide
```
vCenter Server : https://vcsa-01a.corp.local
Username : administrator@corp.local
Password : VMware1!

Inside workshop module
1. Troubleshoot OSPF
2. Troubleshoot DFW
3. Prevent Lateral Movement
```

## Tool for troubleshooting NSX
* TraceFlow
* Flow Monitor
* EndPoint Monitoring
* Central CLI
* Application Rule Manager (ARM)
* LogInsight (Bundled with NSX)

## Lab Topology
![](1892-01-lab-topology-1.png)

